const mongoConfig = require('../config/index');
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const bodyparse = app.use(bodyparser.json());
const server = app.listen(mongoConfig.httpPort);

module.exports = server;
module.exports.bodyparse = bodyparse;
module.exports.app = app;
