const mongoose = require('mongoose');
const { mongoConfig } = require('../config');
const uri = mongoConfig.url;
mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);
mongoose.connect(uri, { useNewUrlParser: true }, (err, db) => {
 if (err) {
   console.log(err, 'err in connecting mongodb');
 } else {
   console.log('connected');
 }
}).catch((error) => {
 console.log(error, 'Error');
});
module.exports = mongoose;
module.exports.db = mongoose.db;
