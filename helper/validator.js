const Response = require('../helper/responses');
const bcrypt = require('bcryptjs');
const Joi = require('joi');
const nameSchema = Joi.object().keys({
  firstName: Joi.string().trim().required().regex(/^[A-Za-z]+$/).error(new Error('FirstName is required')),
  lastName: Joi.string().trim().required().regex(/^[A-Za-z]+$/).error(new Error('LastName is required')),
  middleName: Joi.string().trim().required().regex(/^[A-Za-z]+$/).error(new Error('MiddletName is required'))
});
const houseSchema = Joi.object().keys({
  houseName: Joi.string().trim().error(new Error('House name is required')),
  inYear: Joi.number().min(1966).required().error(new Error('In year is not valid'))
});
const addrSchema = Joi.object().keys({
  houseNo: Joi.string().trim(),
  apartmentName: Joi.string().trim(),
  streetName: Joi.string().trim(),
  cityName: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('City Name is invalid')),
  state: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('State Name is invalid')),
  country: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('Country Name is invalid'))
});
const offaddrSchema = Joi.object().keys({
  buildingName: Joi.string().trim(),
  streetName: Joi.string().trim(),
  cityName: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('City Name is invalid')),
  state: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('State Name is invalid')),
  country: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('Country Name is invalid'))
});
const userSchema = Joi.object().keys({
  isBatchAdmin: Joi.boolean().invalid(false).error(new Error('User Role is not specified')),
  rollNo: Joi.string().required().error(new Error('Roll No is required')),
  enrollmentYear: Joi.number().integer().min(1966).required().error(new Error('Enrollment Year is required')),
  passoutYear: Joi.number().integer().min(1970).required().error(new Error('Passout Year is required')),
  house: houseSchema,
  name: nameSchema,
  password: Joi.string().trim().required().error(new Error('password is required')),
  address: addrSchema,
  email: Joi.string().trim().email().required().error(new Error('Email is required')),
  contactNo: Joi.number().integer().error(new Error('Conatct No is invalid')),
  profession: Joi.string().trim().required().error(new Error('Profession is required')),
  company: Joi.string().trim().required().error(new Error('Company is required')),
  designation: Joi.string().trim().regex(/^[A-Za-z]+$/).error(new Error('Designation is invalid')),
  officeAddress: offaddrSchema,
  birthDate: Joi.string().required().error(new Error('Birth Date is required')),
  bloodGroup: Joi.string().trim(),
  lifeMember: Joi.boolean().invalid(false),
  lifeMemberFrom: Joi.number().integer().min(1966).error(new Error('Member From Year is invalid')),
  maritalStatus: Joi.string().trim().required().error(new Error('Marital Status is required')),
  spouseName: nameSchema,
  childrenName: Joi.array().items(Joi.string().trim()).error(new Error('Children name is invalid'))
}).with('email', 'password');

class Validator {
  async validateBatchAdmin(req, res, next) {
    try {
      await Joi.validate(req.body, userSchema);
      next();
    } catch (e) {
      console.log(e.message, 'err in validator');
      Response.sendError(res, e.message, true);
    }
  }
}
module.exports = new Validator();
