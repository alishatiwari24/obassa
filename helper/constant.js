module.exports = {
  errorCodes: {
    101: { status: 401, message: 'Username is incorrect' },
    102: { status: 401, message: 'Password is incorrect' },
    103: { status: 409, message: 'Duplicate Rollno or Email Entered' }
  }
};
