const User = require('../models/user');
const bcrypt = require('bcryptjs');

class MapRequests {
  getValues(req, res) {
    const encpassword = bcrypt.hashSync(req.body.password, 10);
    let document = {
      isBatchAdmin: req.body.isBatchAdmin,
      rollNo: req.body.rollNo,
      enrollmentYear: req.body.enrollmentYear,
      passoutYear: req.body.passoutYear,
      house: {
        houseName: req.body.houseName,
        inYear: req.body.inYear
      },
      name: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        middleName: req.body.middleName
      },
      password: `${encpassword}`,
      address: {
        houseNo: req.body.houseNo,
        apartmentName: req.body.apartmentName,
        streetName: req.body.streetName,
        cityName: req.body.cityName,
        state: req.body.state,
        country: req.body.country
      },
      email: req.body.email,
      contactNo: req.body.contactno,
      profession: req.body.profession,
      company: req.body.company,
      designation: req.body.designation,
      officeAddress: {
        buildingName: req.body.buildingName,
        streetName: req.body.streetName,
        cityName: req.body.cityName,
        state: req.body.state,
        country: req.body.country
      },
      birthDate: req.body.birthDate,
      bloodGroup: req.body.bloodGroup,
      lifeMember: req.body.lifeMember,
      lifeMemberFrom: req.body.lifeMemberFrom,
      maritalStatus: req.body.maritalStatus,
      spouseName: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        middleName: req.body.middleName
      },
      childrenName: req.body.childrenName
    }
    return document;
  }
}
module.exports = new MapRequests();
