const { errorCodes } = require('./constant');
class Responses {
  sendResponse(res, data, msg) {
    const result = {
      isError: false,
      data,
      message: msg
    };
    res.send(result);
  }

  sendError(res, msg, sendError = false) {
    // console.log(msg, errorCodes[msg]);
    if (errorCodes[msg]) {
      res.status(errorCodes[msg].status).send({ isError: true, message: errorCodes[msg].message })
    } else if (sendError) {
      res.status(500).send({ isError: true, message: msg });
    } else {
      res.status(500).send({ isError: true, message: 'Something Went Wrong!!!' });
    }
  }
}
 module.exports = new Responses();
