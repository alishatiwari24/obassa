module.exports = {
  mongoConfig: {
    url: 'mongodb://localhost:27017/ObassaDB',
    database: 'ObassaDB'
  },
  application: {
    httpPort: 3000
  }
};
