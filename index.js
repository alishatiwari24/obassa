const userRoute = require('./routes/user');
const { application } = require('./config');
const adminRoute = require('./routes/admin');
const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
// const show = require('./controller/showBatchAdmins');
app.use(cors());
app.use(bodyParser.json());
app.use('/user', userRoute);

app.use('/admin', adminRoute);

app.listen(application.httpPort, () => {
  console.log(`server started at - ${application.httpPort}`);
});

// const Admin = require('./insertAdmin');
// const AdminLogin = require('./loginAdmin');
