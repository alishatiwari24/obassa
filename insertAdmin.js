const admin = require('./models/admin');
const bcrypt = require('bcryptjs');
const encpassword = bcrypt.hashSync('admin', 10);
const admin1 = new admin({
  name: 'Admin',
  password: `${encpassword}`
});
admin1.save();
// console.log('Admin inserted');
module.exports.bcrypt = bcrypt;
