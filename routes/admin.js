const express = require('express');
const router = express.Router();
const adminController = require('../controller/admin');
const validator = require('../helper/validator');
const bodyParser = require('body-parser');
router.use(bodyParser.json());
// const MapRequests = require('../helper/mapper');
router.post('/admin-login', adminController.login);

router.post('/add-batch-admin', (req, res, next) => {
  req.body.isBatchAdmin = true;
  next()
}, validator.validateBatchAdmin, adminController.addUser);

module.exports = router;
