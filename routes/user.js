const express = require('express');
const router = express.Router();
const userController = require('../controller/user');
const validator = require('../helper/validator');
const adminController = require('../controller/admin');
// router.get('/get-users', userController.getUsers);

router.post('/user-login', userController.login);

router.post('/add-student', validator.validateBatchAdmin, adminController.addUser);
module.exports = router;
