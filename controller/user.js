const User = require('../models/user');
const Response = require('../helper/responses');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
// class UserController {
//   getUsers(req, res) {
//     console.log('got one request');
//     User.find({}, (err, users) => {
//       res.send({ users })
//     });
//   }
// }
class UserController {
    async login(req, res) {
      try {
      const emailId = req.body.email;
      const pswd = req.body.password;
      const user = await User.find({ email: `${emailId}` });
      if (user.length > 0) {
        const flag = bcrypt.compareSync(`${pswd}`, user[0].password);
        if (flag == true) {
          if (user[0].isBatchAdmin == true) {
            const data = { emailId, role: 'BatchAdmin' };
            const token = jwt.sign(data, 'valid');
            Response.sendResponse(res, { token }, 'Valid batch-admin');
          } else if (user[0].isBatchAdmin == false) {
            const data = { emailId, role: 'Student' };
            const token = jwt.sign(data, 'valid');
            Response.sendResponse(res, { token }, 'Valid Student');
          }
      } else {
        throw new Error(102);
      }
    } else {
      throw new Error(101);
    }
    } catch (err) {
    Response.sendError(res, err.message, true);
    }
  }
}

module.exports = new UserController();
