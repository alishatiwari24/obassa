const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Response = require('../helper/responses');
const Admin = require('../models/admin');
const User = require('../models/user');
const MapRequests = require('../helper/mapper');
const Joi = require('joi');
class AdminController {
  async login(req, res) {
    try {
      const adname = req.body.name;
      const adpass = req.body.password;
      let token;
      const adminUser = await Admin.find({ name: `${adname}` });
      if (adminUser.length > 0) {
        const flag = bcrypt.compareSync(`${adpass}`, adminUser[0].password);
        if (flag == true) {
          const data = { adname };
          token = jwt.sign(data, 'valid');
          // console.log('Valid Admin, Token generated: ', `${token}`);
          Response.sendResponse(res, { token }, 'Successfull');
        } else {
          throw new Error(102);
        }
      } else {
        throw new Error(101);
      }
    } catch (err) {
      Response.sendError(res, err.message);
    }
  }

  async addUser(req, res) {
    try {
      const obj = MapRequests.getValues(req, res);
      const batchadmin = new User(obj);
      const ins = await batchadmin.save();
      Response.sendResponse(res, { ins }, 'Successfull');
    } catch (err) {
      Response.sendError(res, 103);
    }
  }
}
module.exports = new AdminController();
