const mongoose = require('../lib/mongoose');
const bcrypt = require('bcrypt-nodejs');
const adminSchema = new mongoose.Schema({
  name: { type: String, required: true },
  password: { type: String, required: true }
});

const Admin = mongoose.model('Admin', adminSchema);
module.exports = Admin;
