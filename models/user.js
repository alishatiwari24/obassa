const mongoose = require('../lib/mongoose');
const userSchema = new mongoose.Schema({
  isBatchAdmin: { type: Boolean, default: false },
  rollNo: { type: String, unique: true },
  enrollmentYear: { type: Number, min: 1966 },
  passoutYear: { type: Number, min: 1970 },
  house: {
    houseName: { type: String },
    inYear: { type: Number }
  },
  name: {
    firstName: { type: String },
    lastName: { type: String },
    middleName: { type: String }
  },
  password: { type: String, required: true },
  address: {
    houseNo: { type: String },
    apartmentName: { type: String },
    streetName: { type: String },
    cityName: { type: String },
    state: { type: String },
    country: { type: String }
  },
  email: { type: String, unique: true, required: true },
  contactNo: { type: String },
  profession: { type: String },
  company: { type: String },
  designation: { type: String },
  officeAddress: {
    buildingName: { type: String },
    streetName: { type: String },
    cityName: { type: String },
    state: { type: String },
    country: { type: String }
  },
  birthDate: { type: String },
  bloodGroup: { type: String },
  lifeMember: { type: Boolean, default: false },
  lifeMemberFrom: { type: Date },
  maritalStatus: { type: String },
  spouseName: {
    firstName: { type: String },
    lastName: { type: String },
    middleName: { type: String }
  },
  childrenName: { type: String }
});
const User = mongoose.model('User', userSchema);
module.exports = User;
